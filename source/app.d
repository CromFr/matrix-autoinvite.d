import std.stdio;
import std.file;
import std.algorithm;
import core.thread;
import vibe.vibe;

void main()
{
	// Config parsing
	static struct Config {
		string homeserver;
		string token;
		struct Filter {
			enum Type { whitelist, blacklist }
			@byName Type type;
			string[] list;
		}
		Filter filter;
		ubyte[string] invite;
	}
	const config = "config.json".readText.deserializeJson!Config;

	void requestMatrix(in HTTPMethod method, in string path, in string bodyData, void delegate(scope HTTPClientResponse res) callback){
		requestHTTP(config.homeserver ~ path,
			(scope HTTPClientRequest req) {
				req.method = method;
				req.headers["Authorization"] = "Bearer " ~ config.token;
				if(bodyData !is null)
					req.writeBody(cast(ubyte[])bodyData);
			},
			callback);
	}


	bool[string] processedRooms;

	while(1){
		try{
			requestMatrix(HTTPMethod.GET, "/_matrix/client/r0/joined_rooms", null,
				(scope HTTPClientResponse res) {
					enforce(res.statusCode == 200, "Cannot get joined rooms");

					auto data = res.bodyReader.readAllUTF8().parseJsonString;

					foreach(ref roomJson ; data["joined_rooms"].get!(Json[])){
						auto room = roomJson.get!string;

						// Filter rooms
						final switch(config.filter.type) with(Config.Filter.Type){
							case whitelist:
								if(config.filter.list.find(room).empty)
									continue;
								break;
							case blacklist:
								if(!config.filter.list.find(room).empty)
									continue;
								break;
						}

						if(room !in processedRooms){
							writeln("New room: ", room);

							requestMatrix(HTTPMethod.GET, "/_matrix/client/r0/rooms/"~room~"/joined_members", null,
								(scope HTTPClientResponse res) {
									auto resData = res.bodyReader.readAllUTF8().parseJsonString;

									bool[string] present;
									foreach(ref user, pl ; config.invite)
										present[user] = false;

									foreach(user, data ; resData["joined"].get!(Json[string])){
										if(auto u = user in present)
											*u = true;
									}

									foreach(ref user, pres ; present){
										if(pres == false){
											writeln("Invite ", user, " to ", room);

											auto bodyData = Json([
												"user_id": Json(user),
											]);
											requestMatrix(HTTPMethod.POST, "/_matrix/client/r0/rooms/"~room~"/invite", bodyData.toString,
												(scope HTTPClientResponse res) {
													enforce(res.statusCode == 200, "Invite failed with "~res.statusPhrase);

												});


											requestMatrix(HTTPMethod.GET, "/_matrix/client/r0/rooms/"~room~"/state/m.room.power_levels", null,
												(scope HTTPClientResponse res) {
													enforce(res.statusCode == 200, "Get m.room.power_levels failed with "~res.statusPhrase);

													auto pl = res.bodyReader.readAllUTF8().parseJsonString;
													pl["users"][user] = Json(config.invite[user]);

													requestMatrix(HTTPMethod.PUT, "/_matrix/client/r0/rooms/"~room~"/state/m.room.power_levels", pl.toString,
														(scope HTTPClientResponse res) {
															enforce(res.statusCode == 200, "Set m.room.power_levels failed with "~res.statusPhrase);
														});
												});

										}
									}
								});

							processedRooms[room] = true;
						}
					}
				}
			);
		}
		catch(Exception e) writeln(e);

		stdout.flush();
		Thread.sleep(dur!"seconds"(60));
	}
}
