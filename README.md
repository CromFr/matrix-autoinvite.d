

```json
{
    "homeserver": "https://matrix.org",
    "token": "TOKEN",

    // Rooms to include / ignore
    "filter": {
        // whitelist or blacklist
        "type": "blacklist",
        // List of room ID to include in the white/black list
        "list": []
    },
    // Users to invite, with their power level
    "invite": {
        "@CromFr:matrix.org": 100
    }
}
```